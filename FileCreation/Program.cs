﻿string parent = Path.GetDirectoryName(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName);
string fileName = Path.Combine(parent, "text.txt");

Console.Write("Text to add to file (double enter to complete input):\n>>> ");
try
{
    StreamWriter streamWriter = new StreamWriter(fileName);
    string? textLine;
    do
    {
        textLine = Console.ReadLine();
        streamWriter.WriteLine(textLine);
    }                         
    while (textLine != "");
    streamWriter.Close();
}
catch (Exception e)
{
    Console.WriteLine("Exception: " + e.Message);
}
finally
{
    Console.WriteLine("Your text has been successfully written to text.txt");
}
