﻿string parent = Path.GetDirectoryName(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName);
string fileName = Path.Combine(parent, "text.txt");

try
{
    Console.WriteLine("text.txt file content:\n");
    foreach (string line in File.ReadLines(fileName))
    {
        Console.WriteLine(line);
    }
}
catch (Exception e)
{
    Console.WriteLine("Exception: " + e.Message);
}
